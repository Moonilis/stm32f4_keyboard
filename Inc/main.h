/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <stdio.h>
#include "xpt2046_spi.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
extern const uint8_t font8x8[][8];
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void Error_Handler(void);

void TFT_Set_XY(uint16_t x, uint16_t y);
void TFT_Set_Work_Area(uint16_t x, uint16_t y, uint16_t length, uint16_t width);

void TFT_Clear_Screen(uint16_t color);
void TFT_Draw_Char(uint16_t x, uint16_t y, uint16_t color, uint16_t phone, const uint8_t *table, uint8_t ascii, uint8_t size);
void TFT_Draw_String(uint16_t x, uint16_t y, uint16_t color, uint16_t phone, const uint8_t *table, char *string, uint8_t size);

void TFT_Draw_Line (uint16_t x1,uint16_t y1,uint16_t x2,uint16_t y2,uint16_t color, uint8_t size);
void TFT_Draw_HLine(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint16_t color);
void TFT_Draw_VLine(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint16_t color);

void TFT_Draw_Rectangle(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint8_t size, uint16_t color);
void TFT_Draw_Fill_Rectangle(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint16_t color);

void TFT_Draw_Round_Rect(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint16_t r, uint8_t size, uint16_t color);
void TFT_Draw_Fill_Round_Rect(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint16_t r, uint16_t color);

void TFT_Draw_Triangle( uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, uint8_t size, uint16_t color);
void TFT_Draw_Circle(uint16_t x, uint16_t y, uint8_t radius, uint8_t fill, uint8_t size, uint16_t color);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BL_Pin GPIO_PIN_13
#define BL_GPIO_Port GPIOB
#define reset_Pin GPIO_PIN_15
#define reset_GPIO_Port GPIOB
#define T_CS_Pin GPIO_PIN_2
#define T_CS_GPIO_Port GPIOD
#define T_PENIRQ_Pin GPIO_PIN_7
#define T_PENIRQ_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define LCD_DATA (*(volatile uint8_t*)0x6C000001)
#define LCD_REG  (*(volatile uint8_t*)0x6C000000)
#define MAX_WIDTH  479
#define MAX_HEIGHT 319

#define LV_KB_CTRL_BTN_FLAGS 0x0110

#define BLACK     0x00
#define GREEN     0x02
#define DARK_BLUE 0x03
#define BLUE      0x1C
#define WHITE     0xFF
#define YELLOW    0x1F
#define RED       0xE0


#define LCD_NOOP                0x00   /* No Operation*/
#define LCD_SWRESET             0x01   /* Software Reset */
#define LCD_RDDPM               0x0A   /* Read Display Power Mode */
#define LCD_RDDMADCTL           0x0B   /* Read Display MADCTL */
#define LCD_RDDCOLMOD           0x0C   /* Read Display Pixel Format */
#define LCD_RDDIM               0x0D   /* Read Display Image Format */
#define LCD_RDDSM               0x0E   /* Read Display Signal Mode */
#define LCD_RDDSDR              0x0F   /* Read Display Self-Diagnostic Result */
#define LCD_SPLIN               0x10   /* Enter Sleep Mode */
#define LCD_SLEEP_OUT           0x11   /* Sleep out register */
#define LCD_PTLON               0x12   /* Partial Mode ON */
#define LCD_NORMAL_MODE_ON      0x13   /* Normal Display Mode ON */
#define LCD_DINVOFF             0x20   /* Display Inversion OFF */
#define LCD_DINVON              0x21   /* Display Inversion ON */
#define LCD_GAMMA               0x26   /* Gamma register */
#define LCD_DISPLAY_OFF         0x28   /* Display off register */
#define LCD_DISPLAY_ON          0x29   /* Display on register */
#define LCD_COLUMN_ADDR         0x2A   /* Column address register */
#define LCD_PAGE_ADDR           0x2B   /* Page address register */
#define LCD_GRAM                0x2C   /* GRAM register */
#define LCD_RGBSET              0x2D   /* Color SET */
#define LCD_RAMRD               0x2E   /* Memory Read */
#define LCD_PLTAR               0x30   /* Partial Area */
#define LCD_VSCRDEF             0x33   /* Vertical Scrolling Definition */
#define LCD_TEOFF               0x34   /* Tearing Effect Line OFF */
#define LCD_TEON                0x35   /* Tearing Effect Line ON */
#define LCD_MAC                 0x36   /* Memory Access Control register*/
#define LCD_VSCRSADD            0x37   /* Vertical Scrolling Start Address */
#define LCD_IDMOFF              0x38   /* Idle Mode OFF */
#define LCD_IDMON               0x39   /* Idle Mode ON */
#define LCD_PIXEL_FORMAT        0x3A   /* Pixel Format register */
#define LCD_WRITE_MEM_CONTINUE  0x3C   /* Write Memory Continue */
#define LCD_READ_MEM_CONTINUE   0x3E   /* Read Memory Continue */
#define LCD_SET_TEAR_SCANLINE   0x44   /* Set Tear Scanline */
#define LCD_GET_SCANLINE        0x45   /* Get Scanline */
#define LCD_READ_DDB_START      0xA1   /* Read DDB start */

/* Level 2 Commands */
#define LCD_CMDACCPRTC          0xB0   /* Command Access Protect  */
#define LCD_FRMCTR              0xB3   /* Frame Memory Access and Interface setting  */
#define LCD_DMFMCTR             0xB4   /* Display Mode and Frame Memory Write Mode
setting */
#define LCD_DEVCODERD           0xBF   /* Device code read */
#define LCD_PANEL_DRV_CTL       0xC0   /* Panel Driving Setting */
#define LCD_NORMAL_TIMING_WR    0xC1   /* Display Timing Setting for Normal Mode  */
#define LCD_PARTIAL_TIMING_WR   0xC2   /* Display Timing Setting for Partial Mode  */
#define LCD_IDLE_TIMING_WR      0xC3   /* Display Timing Setting for Idle Mode  */
#define LCD_FR_INV_CTL          0xC5   /* Frame rate and Inversion Control  */
#define LCD_INTERFACE           0xC6   /* Interface Control */
#define LCD_GAMMAWR             0xC8   /* Gamma Setting */
#define LCD_POWER               0xD0   /* POWER CONTROL */
#define LCD_VCOM                0xD1   /* VCOM Control */
#define LCD_NORMAL_PWR_WR       0xD2   /* Power Setting for Normal Mode  */
#define LCD_PARTIAL_PWR_WR      0xD3   /* Power Setting for Partial Mode  */
#define LCD_IDLE_PWR_WR         0xD4   /* Power Setting for Idle Mode  */
#define LCD_NVMEMWR             0xE0   /* NV Memory Write  */
#define LCD_NVMEMCTRL           0xE1   /* NV Memory Control */
#define LCD_NVMEMRD             0xE2   /* NV Memory Status */
#define LCD_NVMEMPRT            0xE3   /* NV Memory Protection  */
#define LCD_EEPROMWR_ENABLE     0xE8   /* EEPROM Write Enable  */
#define LCD_EEPROMWR_DISABLE    0xE9   /* EEPROM Write Disable  */
#define LCD_EEPROMWR            0xEA   /* EEPROM Word Write */
#define LCD_EEPROMRD            0xEB   /* EEPROM Word Read   */
#define LCD_EEPROM_ADR_SET      0xEC   /* EEPROM Address Set */
#define LCD_EEPROM_ERASE        0xED   /* EEPROM Erase */
#define LCD_EEPROM_ERASE_ALL    0xEE   /* EEPROM Erase All  */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
