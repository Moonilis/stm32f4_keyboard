#ifndef _XPT2046_H_
#define _XPT2046_H_

#include "stm32f4xx_hal.h"
#include "main.h"
#include <stdbool.h>

/*** Redefine if necessary ***/

// Warning! Use SPI bus with < 2.5 Mbit speed, better ~650 Kbit to be save.
#define XPT2046_SPI_PORT hspi1
extern SPI_HandleTypeDef XPT2046_SPI_PORT;

#define XPT2046_IRQ_Pin       T_PENIRQ_Pin
#define XPT2046_IRQ_GPIO_Port T_PENIRQ_GPIO_Port
#define XPT2046_CS_Pin        T_CS_Pin
#define XPT2046_CS_GPIO_Port  T_CS_GPIO_Port

// change depending on screen orientation
#define XPT2046_SCALE_X 479
#define XPT2046_SCALE_Y 319

// to calibrate uncomment UART_Printf line in ili9341_touch.c

// call before initializing any SPI devices
uint16_t Get_Touch_X(uint8_t);
uint16_t Get_Touch_Y(uint8_t);
bool XPT2046_TouchPressed();
static void XPT2046_TouchUnselect();
#endif

