/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "main.h"
#include "usb_device.h"
#include "usbd_hid.h"
#include "lvgl.h"
#include "lv_conf.h"
#include "lv_porting/lv_port_indev.h"
#include "lv_font/lv_symbol_def.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
SRAM_HandleTypeDef hsram1;

/* USER CODE BEGIN PV */

uint8_t  temp_x = 0, temp_y = 0, but = 0;
bool first_tap = true;
uint8_t buffer[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

static lv_group_t * g;                  /*An Object Group*/
static lv_indev_t * emulated_kp_indev;  /*The input device of the emulated keypad*/
extern USBD_HandleTypeDef hUsbDeviceFS;

static const char * kb_map_lc[] = {"1#", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "Bksp", "\n",
                                   "ABC", "a", "s", "d", "f", "g", "h", "j", "k", "l", "Enter", "\n",
                                   "_", "-", "z", "x", "c", "v", "b", "n", "m", ".", ",", ":", "\n",
                                   LV_SYMBOL_CLOSE, LV_SYMBOL_LEFT, " ", LV_SYMBOL_RIGHT, LV_SYMBOL_OK, ""};

static const lv_btnm_ctrl_t kb_ctrl_lc_map[] = {
    LV_KB_CTRL_BTN_FLAGS | 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 7,
    LV_KB_CTRL_BTN_FLAGS | 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    LV_KB_CTRL_BTN_FLAGS | 2, 2, 6, 2, LV_KB_CTRL_BTN_FLAGS | 2};

static const char * kb_map_uc[] = {"1#", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "Bksp", "\n",
                                   "abc", "A", "S", "D", "F", "G", "H", "J", "K", "L", "Enter", "\n",
                                   "_", "-", "Z", "X", "C", "V", "B", "N", "M", ".", ",", ":", "\n",
                                   LV_SYMBOL_CLOSE, LV_SYMBOL_LEFT, " ", LV_SYMBOL_RIGHT, LV_SYMBOL_OK, ""};

static const lv_btnm_ctrl_t kb_ctrl_uc_map[] = {
    LV_KB_CTRL_BTN_FLAGS | 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 7,
    LV_KB_CTRL_BTN_FLAGS | 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    LV_KB_CTRL_BTN_FLAGS | 2, 2, 6, 2, LV_KB_CTRL_BTN_FLAGS | 2};

static const char * kb_map_spec[] = {"0", "1", "2", "3", "4" ,"5", "6", "7", "8", "9", "Bksp", "\n",
                                     "abc", "+", "-", "/", "*", "=", "%", "!", "?", "#", "<", ">", "\n",
                                     "\\",  "@", "$", "(", ")", "{", "}", "[", "]", ";", "\"", "'", "\n",
                                     LV_SYMBOL_CLOSE, LV_SYMBOL_LEFT, " ", LV_SYMBOL_RIGHT, LV_SYMBOL_OK, ""};

static const lv_btnm_ctrl_t kb_ctrl_spec_map[] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, LV_KB_CTRL_BTN_FLAGS | 2,
    LV_KB_CTRL_BTN_FLAGS | 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    LV_KB_CTRL_BTN_FLAGS | 2, LV_KB_CTRL_BTN_FLAGS | 2, 6, 2, 2};

static const char * kb_map_num[] = {"1", "2", "3", LV_SYMBOL_CLOSE, "\n",
                                    "4", "5", "6", LV_SYMBOL_OK, "\n",
                                    "7", "8", "9", "Bksp", "\n",
                                    "+/-", "0", ".", LV_SYMBOL_LEFT, LV_SYMBOL_RIGHT, ""};

static const lv_btnm_ctrl_t kb_ctrl_num_map[] = {
        1, 1, 1, LV_KB_CTRL_BTN_FLAGS | 2,
        1, 1, 1, LV_KB_CTRL_BTN_FLAGS | 2,
        1, 1, 1, LV_KB_CTRL_BTN_FLAGS | 2,
        LV_KB_CTRL_BTN_FLAGS | 1, 1, 1, LV_KB_CTRL_BTN_FLAGS | 1, LV_KB_CTRL_BTN_FLAGS | 1};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_FSMC_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */

__inline void TFT_Send_Cmd(uint8_t reg)
{
	LCD_REG = reg;
}
////////////////////////
__inline void TFT_Write_Data(uint8_t data)
{
	LCD_DATA = data;
}

void keyboard_write(uint8_t char1){
    buffer[2]=char1;
    USBD_HID_SendReport(&hUsbDeviceFS,buffer,sizeof(buffer));
    HAL_Delay(100);
    for (int i = 0; i < 9; ++i)
    {
    	buffer[i] = 0;
    }
    USBD_HID_SendReport(&hUsbDeviceFS,buffer,sizeof(buffer));
    HAL_Delay(100);
}

void LCD_Init(void)
    {
       //************* Start Initial Sequence **********//
	   HAL_Delay(100);
       HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);
       HAL_Delay(100);
	   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);
	   HAL_Delay(100);
	   HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);
	   HAL_Delay(100);


	   TFT_Send_Cmd(0x11);
		HAL_Delay(20);
		TFT_Send_Cmd(0xD0);
		TFT_Write_Data(0x07);
		TFT_Write_Data(0x47);
		TFT_Write_Data(0x16);

		TFT_Send_Cmd(0xD1);
		TFT_Write_Data(0x00);
		TFT_Write_Data(0x07);
		TFT_Write_Data(0x10);

		TFT_Send_Cmd(0xD2);
		TFT_Write_Data(0x01);
		TFT_Write_Data(0x02);


		TFT_Send_Cmd(0xC5);
		TFT_Write_Data(0x0);//mod 0x03


	    /*TFT_Send_Cmd(0x11); // exit sleep mode
	    HAL_Delay(100);*/


		/*TFT_Send_Cmd(0xC0); // panel driving settings
		TFT_Write_Data(0x00); // 10
		TFT_Write_Data(0x3B);
		TFT_Write_Data(0x00);
		TFT_Write_Data(0x02);
		TFT_Write_Data(0x11); // 11*/

		TFT_Send_Cmd(0xC8);
		TFT_Write_Data(0x00);
		TFT_Write_Data(0x32);
		TFT_Write_Data(0x36);
		TFT_Write_Data(0x45);
		TFT_Write_Data(0x06);
		TFT_Write_Data(0x16);
		TFT_Write_Data(0x37);
		TFT_Write_Data(0x75);
		TFT_Write_Data(0x77);
		TFT_Write_Data(0x54);
		TFT_Write_Data(0x0C);
		TFT_Write_Data(0x00);

		TFT_Send_Cmd(0x36); //Screen Rotate
		TFT_Write_Data(0x2B);

		TFT_Send_Cmd(0x3A);
		TFT_Write_Data(0x55); // 55

		HAL_Delay(200);
		TFT_Send_Cmd(0x29);

		TFT_Send_Cmd(0x21);
}


void TFT_Set_X(uint16_t start_x,uint16_t end_x)
{
	TFT_Send_Cmd(0x2A);
	TFT_Write_Data(start_x>>8);
	TFT_Write_Data(start_x); //&0x00ff
	TFT_Write_Data(end_x>>8);
	TFT_Write_Data(end_x);
}


void TFT_Set_Y(uint16_t start_y,uint16_t end_y)
{
	TFT_Send_Cmd(0x2B);
	TFT_Write_Data(start_y>>8);
	TFT_Write_Data(start_y);
	TFT_Write_Data(end_y>>8);
	TFT_Write_Data(end_y);
}

void TFT_Set_XY(uint16_t x, uint16_t y)
{
	TFT_Set_X(x, x);
	TFT_Set_Y(y, y);
	TFT_Send_Cmd(0x2C);
}

void TFT_Set_Work_Area(uint16_t x, uint16_t y, uint16_t length, uint16_t width)
{
	TFT_Set_X(x, x+length);
	TFT_Set_Y(y, y+width);
	TFT_Send_Cmd(0x2C);
}

void TFT_Clear_Screen(uint16_t color)
{
	uint32_t i=0;
	TFT_Set_Work_Area(0, 0, MAX_WIDTH, MAX_HEIGHT);
	for(i=0; i < 153600; i++)
	{
		TFT_Write_Data(color>>8);
		TFT_Write_Data(color&0xFF);
	}
}

void TFT_Draw_Char(uint16_t x, uint16_t y, uint16_t color, uint16_t phone,const uint8_t *table, uint8_t ascii, uint8_t size)
{
	uint8_t i,f = 0;


	for (i = 0; i < 8; i++)
	{
		for(f = 0; f < 8; f++)
		{
			if((*(table + 8*(ascii-0x20)+i)>>(7-f))&0x01)
			{
				 TFT_Draw_Fill_Rectangle(x+f*size, y+i*size, size, size, color);
			}
			else
			{
				 TFT_Draw_Fill_Rectangle(x+f*size, y+i*size, size, size, phone);
			}
		}
	}
}

void TFT_Draw_String(uint16_t x, uint16_t y, uint16_t color,uint16_t phone, const uint8_t *table, char *string, uint8_t size)
{

	while(*string)
	{

		if((x + 8) > MAX_WIDTH)
		{
			x = 1;
			y = y + 8*size;
		}
		TFT_Draw_Char(x, y, color, phone, table, *string, size);
		x += 8*size;
		*string++;
	}
}

void TFT_Draw_Line (uint16_t x1,uint16_t y1,uint16_t x2,uint16_t y2, uint16_t color, uint8_t size)
{
	int deltaX = abs(x2 - x1);
	int deltaY = abs(y2 - y1);
	int signX = x1 < x2 ? 1 : -1;
	int signY = y1 < y2 ? 1 : -1;
	int error = deltaX - deltaY;
	int error2 = 0;

	for (;;)
	{
		TFT_Draw_Fill_Rectangle(x1,y1,size,size,color);

		if(x1 == x2 && y1 == y2)
			break;

		error2 = error * 2;

		if(error2 > -deltaY)
		{
			error -= deltaY;
			x1 += signX;
		}

		if(error2 < deltaX)
		{
			error += deltaX;
			y1 += signY;
		}
	}
}


void TFT_Draw_HLine(uint16_t x, uint16_t y, uint16_t length, uint16_t size, uint16_t color)
{
	uint16_t i=0;

	TFT_Set_Work_Area(x,y,length,size);
	for(i=0; i<(length*size); i++)
	{
		TFT_Write_Data(color>>8);
		TFT_Write_Data(color&0xFF);
	}
}


void TFT_Draw_VLine(uint16_t x, uint16_t y, uint16_t length, uint16_t size, uint16_t color)
{
	uint16_t i=0;

	TFT_Set_Work_Area(x,y,size,length);
	for(i=0; i<(length*size); i++)
	{
		TFT_Write_Data(color>>8);
		TFT_Write_Data(color&0xFF);
	}
}

void TFT_Draw_Rectangle(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint8_t size, uint16_t color)
{
	TFT_Draw_HLine(x, y, length, size, color);
	TFT_Draw_HLine(x, y + width, length, size, color);
	TFT_Draw_VLine(x, y, width, size, color);
	TFT_Draw_VLine(x + length - size, y, width, size, color);
}


void TFT_Draw_Fill_Rectangle(uint16_t x, uint16_t y, uint16_t length, uint16_t width, uint16_t color)
{
	uint32_t i=0;

	TFT_Set_Work_Area(x,y,length, width);
	for(i=0; i < length*width; i++)
	{
		TFT_Write_Data(color>>8);
		TFT_Write_Data(color&0xFF);
	}
}


void TFT_Draw_Triangle( uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, uint8_t size, uint16_t color)
{

	TFT_Draw_Line( x1, y1, x2, y2, color, size);
	TFT_Draw_Line( x2, y2, x3, y3, color, size);
	TFT_Draw_Line( x3, y3, x1, y1, color, size);
}

void TFT_Draw_Circle(uint16_t x, uint16_t y, uint8_t radius, uint8_t fill, uint8_t size, uint16_t color)
{
	int a_,b_,P;
	a_ = 0;
	b_ = radius;
	P = 1 - radius;
	while (a_ <= b_)
	{
		if(fill == 1)
		{
			TFT_Draw_Fill_Rectangle(x-a_,y-b_,2*a_+1,2*b_+1,color);
			TFT_Draw_Fill_Rectangle(x-b_,y-a_,2*b_+1,2*a_+1,color);
		}
		else
		{
			TFT_Draw_Fill_Rectangle(a_+x, b_+y, size, size, color);
			TFT_Draw_Fill_Rectangle(b_+x, a_+y, size, size, color);
			TFT_Draw_Fill_Rectangle(x-a_, b_+y, size, size, color);
			TFT_Draw_Fill_Rectangle(x-b_, a_+y, size, size, color);
			TFT_Draw_Fill_Rectangle(b_+x, y-a_, size, size, color);
			TFT_Draw_Fill_Rectangle(a_+x, y-b_, size, size, color);
			TFT_Draw_Fill_Rectangle(x-a_, y-b_, size, size, color);
			TFT_Draw_Fill_Rectangle(x-b_, y-a_, size, size, color);
		}
		if (P < 0 )
		{
			P = (P + 3) + (2* a_);
			a_ ++;
		}
		else
		{
			P = (P + 5) + (2* (a_ - b_));
			a_ ++;
			b_ --;
		}
	}
}
//---------------------------------������� ������������-------------------------------------------//
//static void Move_Mouse(volatile uint8_t *xp, volatile uint8_t *yp)
//{
//	if (first_tap)
//	{
//		temp_x = *xp;  temp_y = *yp;
//		first_tap = false;
//	}
//	else if (temp_x != *xp && temp_y != *yp)
//	{
//	   if (temp_x - *xp > 0)     { HID_Buffer[1] = -3;}
//	   else if (temp_x - *xp < 0){ HID_Buffer[1] =  3;}
//
//	   if (temp_y - *yp > 0)     { HID_Buffer[2] = -3;}
//	   else if (temp_y - *yp < 0){ HID_Buffer[2] =  3;}
//
//	   USBD_HID_SendReport(&hUsbDeviceFS, HID_Buffer, 4);
//	   temp_x = *xp; temp_y = *yp;
//	}
//}

void LCD_WritePixel(uint16_t x, uint16_t y, uint16_t color) {
	TFT_Set_Work_Area(x, y, 1, 1);
	TFT_Write_Data(color>>8);
	TFT_Write_Data(color&0xFF);
}

void my_disp_flush(lv_disp_t * disp, const lv_area_t * area, lv_color_t * color_p){
    /*The most simple case (but also the slowest) to put all pixels to the screen one-by-one*/
	int32_t x, y;
	    for(y = area->y1; y <= area->y2; y++) {
	        for(x = area->x1; x <= area->x2; x++) {
	        	LCD_WritePixel(x, y, lv_color_to16(*color_p));  /* Put a pixel to the display.*/
	            color_p++;
	        }
	    }

	    lv_disp_flush_ready(disp);         /* Indicate you are ready with the flushing*/
}

bool my_touchpad_read(lv_indev_t * indev, lv_indev_data_t * data)
{
    /*static lv_coord_t last_x = 0;
    static lv_coord_t last_y = 0;*/

	uint32_t last_x=0, last_y=0;

    /*Save the state and save the pressed coordinate*/
    data->state = XPT2046_TouchPressed() ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
    if(data->state == LV_INDEV_STATE_PR) Get_Touch_xy(&last_x, &last_y, 20);

    /*Set the coordinates (if released use the last pressed coordinates)*/
    data->point.x = last_x;
    data->point.y = last_y;

    return false; /*Return `false` because we are not buffering and no more data to read*/
}

void LV_Init(void) {

	  lv_init();

  	  /*A static or global variable to store the buffers*/
      static lv_disp_buf_t disp_buf;

      /*Static or global buffer(s). The second buffer is optional*/
      static lv_color_t buf_1[LV_HOR_RES_MAX * 10];
      static lv_color_t buf_2[LV_HOR_RES_MAX * 10];

      /*Initialize `disp_buf` with the buffer(s) */
      lv_disp_buf_init(&disp_buf, buf_1, buf_2, LV_HOR_RES_MAX*10);

      lv_disp_drv_t disp_drv;               /*Descriptor of a display driver*/
      lv_disp_drv_init(&disp_drv);          /*Basic initialization*/
      disp_drv.flush_cb = my_disp_flush;    /*Set your driver function*/
      disp_drv.buffer = &disp_buf;          /*Assign the buffer to the display*/
      lv_disp_t * disp;
      disp = lv_disp_drv_register(&disp_drv);      /*Finally register the driver*/

      /*Register the emulated keyboard*/
      lv_indev_drv_t  kp_drv;
      lv_indev_drv_init(&kp_drv);
      kp_drv.type = LV_INDEV_TYPE_POINTER;
      kp_drv.read_cb = my_touchpad_read;
      emulated_kp_indev = lv_indev_drv_register(&kp_drv);

      /*Create an object group*/
      g = lv_group_create();

      /*Assig the input device(s) to the created group*/
      lv_indev_set_group(emulated_kp_indev, g);
}

void keyboard_event(lv_obj_t * kb, lv_event_t event)
{
	if(event != LV_EVENT_VALUE_CHANGED && event != LV_EVENT_LONG_PRESSED_REPEAT) return;

	    lv_kb_ext_t * ext = lv_obj_get_ext_attr(kb);
	    uint16_t btn_id   = lv_btnm_get_active_btn(kb);
	    if(btn_id == LV_BTNM_BTN_NONE) return;
	    if(lv_btnm_get_btn_ctrl(kb, btn_id, LV_BTNM_CTRL_HIDDEN | LV_BTNM_CTRL_INACTIVE)) return;
	    if(lv_btnm_get_btn_ctrl(kb, btn_id, LV_BTNM_CTRL_NO_REPEAT) && event == LV_EVENT_LONG_PRESSED_REPEAT) return;

	    const char * txt = lv_btnm_get_active_btn_text(kb);
	    if(txt == NULL) return;

	    if(strcmp(txt, "abc") == 0) {
	        lv_btnm_set_map(kb, kb_map_lc);
	        lv_btnm_set_ctrl_map(kb, kb_ctrl_lc_map);
	        return;
	    } else if(strcmp(txt, "ABC") == 0) {
	        lv_btnm_set_map(kb, kb_map_uc);
	        lv_btnm_set_ctrl_map(kb, kb_ctrl_uc_map);
	        return;
	    } else if(strcmp(txt, "1#") == 0) {
	        lv_btnm_set_map(kb, kb_map_spec);
	        lv_btnm_set_ctrl_map(kb, kb_ctrl_spec_map);
	        return;
	    } else if(strcmp(txt, LV_SYMBOL_CLOSE) == 0) {
			keyboard_write(0x29); // ESC ���� �����
			return;
	    } else if(strcmp(txt, LV_SYMBOL_OK) == 0) {
	        return; // �� � �� ��� ����� �������?
	    } else if(strcmp(txt, " ") == 0) {
	    	keyboard_write(0x2C); // ESC ���� �����
	        return; // �� � �� ��� ����� �������?
	    }
	    if(strcmp(txt, "Enter") == 0)
	    	keyboard_write(0x28); 			// Enter �� � ���� Enter
	    else if(strcmp(txt, LV_SYMBOL_LEFT) == 0)
	    	keyboard_write(0x50); 			// ����
	    else if(strcmp(txt, LV_SYMBOL_RIGHT) == 0)
	    	keyboard_write(0x4F); 			// �����
	    else if(strcmp(txt, "Bksp") == 0)
	    	keyboard_write(0x2A); 			// ����� �����
        else {
        	uint8_t temp = *txt;
        	if (0x41 <= temp && temp <= 0x5A)
        	{
        		temp -= 61;
        		buffer[0] = 0x02;
            	keyboard_write(temp);
        	}
        	else if ( 0x61 <= temp && temp <= 0x7A)
        	{
        		temp -= 93;
            	keyboard_write(temp);
        	}
        	else if ( 0x31 <= temp && temp <= 0x39)
        	{
        		temp -= 19;
            	keyboard_write(temp);
        	}
        	return;
	    }
}

void lv_tutorial_keyboard()
{
	static lv_style_t rel_style, pr_style;

	    lv_style_copy(&rel_style, &lv_style_btn_rel);
	    rel_style.body.radius = 0;
	    rel_style.body.border.width = 1;

	    lv_style_copy(&pr_style, &lv_style_btn_pr);
	    pr_style.body.radius = 0;
	    pr_style.body.border.width = 1;

	    /*Create a keyboard and apply the styles*/
	    lv_obj_t *kb = lv_kb_create(lv_scr_act(), NULL);
	    lv_obj_set_event_cb(kb, keyboard_event);
	    lv_kb_set_cursor_manage(kb, true);
	    lv_kb_set_style(kb, LV_KB_STYLE_BG, &lv_style_transp_tight);
	    lv_kb_set_style(kb, LV_KB_STYLE_BTN_REL, &rel_style);
	    lv_kb_set_style(kb, LV_KB_STYLE_BTN_PR, &pr_style);

}

static void GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_14;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_11 | GPIO_PIN_12;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET);
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  GPIO_Init();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_FSMC_Init();
  MX_SPI1_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
  LV_Init();
  LCD_Init();
//  lv_port_indev_init();
  TFT_Clear_Screen(WHITE);
  lv_tutorial_keyboard();

  /* USER CODE END 2 */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint32_t ax, ay;

  //Touch_Calibrate();

//	extern lv_font_t lv_font_roboto_12;
//	static lv_style_t style_diagText;
//	lv_style_copy(&style_diagText, &lv_style_plain);
//	//style_HumText.text.font = &roboto_bold_85;
//	style_diagText.text.font = &lv_font_roboto_12;
//	style_diagText.text.color = LV_COLOR_MAKE(0x00,0x00,0x00);
//	style_diagText.text.letter_space = 2;
//
//	char coord_txt[30];
//	sprintf(coord_txt,"X: %d, Y: %d",ax,ay);
//
//	lv_obj_t * blabla = lv_label_create(lv_scr_act(), NULL);
//	lv_obj_set_style(blabla, &style_diagText);
//	lv_label_set_text(blabla, coord_txt);
//	lv_obj_align(blabla, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);

  while (1)
  {
//	  if (XPT2046_TouchPressed())
//	  {
//	    Get_Touch_xy(&ax, &ay, 20);
//	    sprintf(coord_txt,"X: %d, Y: %d",ax,ay);
//	    lv_label_set_text(blabla, coord_txt);
//	    lv_obj_align(blabla, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);
//	  }
      lv_task_handler();
      lv_tick_inc(1);
      //HAL_Delay(1);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLQ;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, BL_Pin|T_PENIRQ_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(reset_GPIO_Port, reset_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(T_CS_GPIO_Port, T_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : BL_Pin */
  GPIO_InitStruct.Pin = BL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(BL_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : reset_Pin */
  GPIO_InitStruct.Pin = reset_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(reset_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : T_CS_Pin */
  GPIO_InitStruct.Pin = T_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(T_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : T_PENIRQ_Pin */
  GPIO_InitStruct.Pin = T_PENIRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(T_PENIRQ_GPIO_Port, &GPIO_InitStruct);

}

/* FSMC initialization function */
static void MX_FSMC_Init(void)
{

  /* USER CODE BEGIN FSMC_Init 0 */

  /* USER CODE END FSMC_Init 0 */

  FSMC_NORSRAM_TimingTypeDef Timing = {0};

  /* USER CODE BEGIN FSMC_Init 1 */

  /* USER CODE END FSMC_Init 1 */

  /** Perform the SRAM1 memory initialization sequence
  */
  hsram1.Instance = FSMC_NORSRAM_DEVICE;
  hsram1.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
  /* hsram1.Init */
  hsram1.Init.NSBank = FSMC_NORSRAM_BANK4;
  hsram1.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
  hsram1.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
  hsram1.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_8;
  hsram1.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
  hsram1.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
  hsram1.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
  hsram1.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
  hsram1.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
  hsram1.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
  hsram1.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
  hsram1.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
  hsram1.Init.ContinuousClock = FSMC_CONTINUOUS_CLOCK_SYNC_ONLY;
  hsram1.Init.WriteFifo = FSMC_WRITE_FIFO_DISABLE;
  hsram1.Init.PageSize = FSMC_PAGE_SIZE_NONE;
  /* Timing */
  Timing.AddressSetupTime = 0;
  Timing.AddressHoldTime = 15;
  Timing.DataSetupTime = 3;
  Timing.BusTurnAroundDuration = 0;
  Timing.CLKDivision = 16;
  Timing.DataLatency = 17;
  Timing.AccessMode = FSMC_ACCESS_MODE_A;
  /* ExtTiming */

  if (HAL_SRAM_Init(&hsram1, &Timing, NULL) != HAL_OK)
  {
    Error_Handler( );
  }

  /* USER CODE BEGIN FSMC_Init 2 */

  /* USER CODE END FSMC_Init 2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
