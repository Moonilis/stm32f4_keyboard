#include <stdio.h>
#include <stdlib.h>
#include "xpt2046_spi.h"

#define READ_Y 0xD0
#define READ_X 0x90

static const int16_t xCenter[] = { 35, MAX_WIDTH -35, 35, MAX_WIDTH -35 };
static const int16_t yCenter[] = { 35, 35, MAX_HEIGHT-35, MAX_HEIGHT-35 };

static int16_t xPos[5], yPos[5];

static float axc[2],  ayc[2];
static int16_t bxc[2], byc[2];

float   ax = -7.85,  ay = 11.5;
int16_t bx = 500,  by = -16.5;

static void XPT2046_TouchSelect();

void init_XPT2046(void)
{
	//CS_T_ACTIVE;
	XPT2046_TouchSelect();

	uint8_t tx_data[1];

	tx_data[0] = 0x80;
	HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)tx_data, 1, 100);

	tx_data[0] = 0x00;
	HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)tx_data, 1, 100);

	tx_data[0] = 0x00;
	HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)tx_data, 1, 100);

	//CS_T_IDLE;
	XPT2046_TouchUnselect();
}


static void XPT2046_TouchSelect()
{
    HAL_GPIO_WritePin(XPT2046_CS_GPIO_Port, XPT2046_CS_Pin, GPIO_PIN_RESET);
}

static void XPT2046_TouchUnselect()
{
    HAL_GPIO_WritePin(XPT2046_CS_GPIO_Port, XPT2046_CS_Pin, GPIO_PIN_SET);
}

bool XPT2046_TouchPressed()
{
    return HAL_GPIO_ReadPin(XPT2046_IRQ_GPIO_Port, XPT2046_IRQ_Pin) == GPIO_PIN_RESET;
}

void Touch_Set_Coef ( float _ax, int16_t _bx, float _ay, int16_t _by )
{
	ax = _ax;
	bx = _bx;
	ay = _ay;
	by = _by;
}

uint16_t Get_Touch_X(uint8_t count_read)
{
	uint8_t nsamples = 0;
	uint32_t temp_x = 0, x_kor = 0;
	static const uint8_t cmd_read_x[] = { READ_X };
	static const uint8_t zeroes_tx[] = { 0x00, 0x00 };
    uint32_t avg_x = 0;

    XPT2046_TouchSelect();

    for(uint8_t i = 0; i < count_read; i++)
    {
       if(!XPT2046_TouchPressed())
           break;

		HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)cmd_read_x, sizeof(cmd_read_x), HAL_MAX_DELAY);
		uint8_t x_raw[2];
		HAL_SPI_TransmitReceive(&XPT2046_SPI_PORT, (uint8_t*)zeroes_tx, x_raw, sizeof(x_raw), HAL_MAX_DELAY);

		temp_x = (((uint16_t)x_raw[0]) << 8) | ((uint16_t)x_raw[1]);
		temp_x >>= 3 ;

		if (temp_x < 4090 && temp_x > 0)
		{
			avg_x += temp_x;
			nsamples++;
		}
	}

	XPT2046_TouchUnselect();

	x_kor = (avg_x / nsamples) / ax + bx;

	return x_kor;
}

uint16_t Get_Touch_Y(uint8_t count_read)
{
	uint8_t nsamples = 0;
	uint32_t temp_y = 0, y_kor = 0;
	static const uint8_t cmd_read_y[] = { READ_Y };
	static const uint8_t zeroes_tx[] = { 0x00, 0x00 };
    uint32_t avg_y = 0;

    XPT2046_TouchSelect();

    for(uint8_t i = 0; i < count_read; i++)
    {
       if(!XPT2046_TouchPressed())
           break;

		HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)cmd_read_y, sizeof(cmd_read_y), HAL_MAX_DELAY);
		uint8_t y_raw[2];
		HAL_SPI_TransmitReceive(&XPT2046_SPI_PORT, (uint8_t*)zeroes_tx, y_raw, sizeof(y_raw), HAL_MAX_DELAY);

		temp_y = (((uint16_t)y_raw[0]) << 8) | ((uint16_t)y_raw[1]);
		temp_y >>= 3 ;

		if (temp_y < 4090 && temp_y > 0)
		{
			avg_y += temp_y;
			nsamples++;
		}
	}

	XPT2046_TouchUnselect();

	y_kor = (avg_y / nsamples) / ay + by;

	return y_kor;
}

void Get_Touch_xy(uint32_t *x_kor, uint32_t *y_kor, uint8_t count_read)
{
	uint8_t nsamples = 0;
	uint32_t temp_x = 0, temp_y = 0;

	static const uint8_t cmd_read_x[] = { READ_X };
	static const uint8_t cmd_read_y[] = { READ_Y };
	static const uint8_t zeroes_tx[] = { 0x00, 0x00 };
    uint32_t avg_x = 0;
    uint32_t avg_y = 0;
    XPT2046_TouchSelect();

    for(uint8_t i = 0; i < count_read; i++)
    {
       if(!XPT2046_TouchPressed())
           break;

		HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)cmd_read_y, sizeof(cmd_read_y), HAL_MAX_DELAY);
		uint8_t y_raw[2];
		HAL_SPI_TransmitReceive(&XPT2046_SPI_PORT, (uint8_t*)zeroes_tx, y_raw, sizeof(y_raw), HAL_MAX_DELAY);

		HAL_SPI_Transmit(&XPT2046_SPI_PORT, (uint8_t*)cmd_read_x, sizeof(cmd_read_x), HAL_MAX_DELAY);
		uint8_t x_raw[2];
		HAL_SPI_TransmitReceive(&XPT2046_SPI_PORT, (uint8_t*)zeroes_tx, x_raw, sizeof(x_raw), HAL_MAX_DELAY);

		temp_x = (((uint16_t)x_raw[0]) << 8) | ((uint16_t)x_raw[1]);
		temp_y = (((uint16_t)y_raw[0]) << 8) | ((uint16_t)y_raw[1]);
		temp_x >>= 3 ;
		temp_y >>= 3 ;

		if (temp_x < 4090 && temp_x > 0 && temp_y < 4090 && temp_y > 0 )
		{
			avg_x += temp_x;
			avg_y += temp_y;
			nsamples++;
		}

    }

	XPT2046_TouchUnselect();

	int temp_x_kor, temp_y_kor;

	temp_x_kor = (avg_x / nsamples) / ax + bx;
	temp_y_kor = (avg_y / nsamples) / ay + by;

//	temp_x_kor = (avg_x / nsamples);
//	temp_y_kor = (avg_y / nsamples);

	*x_kor = temp_x_kor;
	*y_kor = temp_y_kor;

//	  char buf[6];
//	  char *buffer = buf;
//	itoa(*x_kor, buffer, 10);
//	TFT_Draw_String(150, 100, BLUE, WHITE,(const uint8_t*) font8x8, buffer, 3);
//	itoa(*y_kor, buffer, 10);
//	TFT_Draw_String(150, 150, BLUE, WHITE,(const uint8_t*) font8x8, buffer, 3);

}

void Touch_Calibrate ( void )
{
	uint32_t x, y;
	//������ ������� � ����� ������� ����
	TFT_Clear_Screen (WHITE);
	TFT_Draw_String( 50, 100, 0xFFE0, BLUE, *font8x8, "Callibration", 3  );
	TFT_Draw_Line ( 10, 10+25, 10+50, 10+25, BLUE,1 );
	TFT_Draw_Line ( 10+25, 10, 10+25, 10+50, BLUE,1 );

	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Press",  3 );
	while (1)
	{
		//����� �������
		while ( !XPT2046_TouchPressed() ){ };
		Get_Touch_xy ( &x, &y, 10);//���������� ��������� 100 ���
		xPos[0] = x;
		yPos[0] = y;
		break;
	}

	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Off   ",  3 );
	//����� ����������
	while ( XPT2046_TouchPressed ( ) );
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8,  "     ", 3 );


	//������ ������� � ������ ������� ����
	TFT_Clear_Screen (WHITE);
	TFT_Draw_String( 50, 100, 0xFFE0, 0x0000, *font8x8, "Callibration", 3 );
	TFT_Draw_Line  ( MAX_WIDTH -10-50, 10+25, MAX_WIDTH -10-50+50, 10+25, BLUE,1 );
	TFT_Draw_Line  ( MAX_WIDTH -10-25, 10, MAX_WIDTH -10-25, 10+50, BLUE,1 );

	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Press",  3 );
	while (1)
	{
		//����� �������
		while ( !XPT2046_TouchPressed ( ) );
		Get_Touch_xy ( &x, &y, 10);//���������� ��������� 100 ���
		xPos[1] = x;
		yPos[1] = y;
		break;
	}
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Off   ",  3 );

	//����� ����������
	while ( XPT2046_TouchPressed( ) );
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8,  "     ", 3 );


	//������ ������� � ����� ������ ����
	TFT_Clear_Screen (WHITE);
	TFT_Draw_String( 50, 100, 0xFFE0, 0x0000, *font8x8, "Callibration", 3 );
	TFT_Draw_Line  ( 10, MAX_HEIGHT-10-25, 10+50, MAX_HEIGHT-10-25, 0x0000,1 );	// hor
	TFT_Draw_Line  ( 10+25, MAX_HEIGHT-10-50, 10+25, MAX_HEIGHT-10-50+50, 0x0000,1 );	// vert

	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Press",  3 );
	while (1)
	{
		// ����� �������
		while ( !XPT2046_TouchPressed ( ) );
		Get_Touch_xy ( &x, &y, 10);//���������� ��������� 100 ���
		xPos[2] = x;
		yPos[2] = y;
		break;
	}
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Off   ",  3 );

	// ����� ����������
	while ( XPT2046_TouchPressed() );
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8,  "     ", 3 );


	//������ ������� � ������ ������ ����
	TFT_Clear_Screen (WHITE);
	TFT_Draw_String( 50, 100, 0xFFE0, 0x0000, *font8x8, "Callibration",  3 );
	TFT_Draw_Line  ( MAX_WIDTH -10-50, MAX_HEIGHT-10-25, MAX_WIDTH -10-50+50, MAX_HEIGHT-10-25, 0x0000,1 );	// hor
	TFT_Draw_Line  ( MAX_WIDTH -10-25, MAX_HEIGHT-10-50, MAX_WIDTH -10-25, MAX_HEIGHT-10-50+50, 0x0000,1 );	// vert

	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Press",  3 );
	while (1)
	{
		// ����� �������
		while ( !XPT2046_TouchPressed ( ) );
		Get_Touch_xy ( &x, &y, 10);
		xPos[3] = x;
		yPos[3] = y;
		break;
	}
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8, "Off   ",  3 );

	//����� ����������
	while ( XPT2046_TouchPressed ( ) );
	TFT_Draw_String( 50, 120, 0xFFE0, 0x0000, *font8x8,  "     ", 3 );


	//������ �������������.
	//xPos - �������� ���������� � ������� ���������x, xCenter - �������� ���������� ����� �� �������
	axc[0] = (float)(xPos[3] - xPos[0])/(xCenter[3] - xCenter[0]);
	bxc[0] = xCenter[0] - xPos[0]/axc[0];
	ayc[0] = (float)(yPos[3] - yPos[0])/(yCenter[3] - yCenter[0]);
	byc[0] = yCenter[0] - yPos[0]/ayc[0];

	/*axc[1] = (float)(xPos[2] - xPos[1])/(xCenter[2] - xCenter[1]);
	bxc[1] = xCenter[1] - xPos[1]/axc[1];
	ayc[1] = (float)(yPos[2] - yPos[1])/(yCenter[2] - yCenter[1]);
	byc[1] = yCenter[1] - yPos[1]/ayc[1];*/

	// ��������� ������������
	Touch_Set_Coef ( axc[0], bxc[0], ayc[0], byc[0] );

	TFT_Clear_Screen (WHITE);
	TFT_Draw_String( 50, 100, 0xFFE0, 0x0000, *font8x8, "Callibration_End",  3 );
	HAL_Delay (1000);	// 1 sec

	char buf[6];
	char *buffer = buf;

	itoa(ax*1000, buffer, 10);
    TFT_Draw_String(0, 50, BLUE, BLACK,(const uint8_t*) font8x8, buffer, 3);
    itoa(ay*1000, buffer, 10);
    TFT_Draw_String(0, 100, BLUE, BLACK,(const uint8_t*) font8x8, buffer, 3);
	itoa(bx, buffer, 10);
    TFT_Draw_String(0, 150, BLUE, BLACK,(const uint8_t*) font8x8, buffer, 3);
    itoa(by, buffer, 10);
    TFT_Draw_String(0, 200, BLUE, BLACK,(const uint8_t*) font8x8, buffer, 3);
}
